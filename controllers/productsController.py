from pkg.custoJusto.custoJustoService import getCustoJustoProducts
from pkg.olx.olxService import getOlxProducts
from pkg.standVirtual.standVirutalService import getStandVirtualProducts
from errorHandler import InvalidUsage

def getProducts(request):
    products = []
    
    prodCj = getCustoJustoProducts(request.get_json())
    prodOlx = getOlxProducts(request.get_json())
    prodSv = getStandVirtualProducts(request.get_json())

    products = prodCj + prodOlx + prodSv
    
    return products

def getProductsOlx(request):
    return getOlxProducts(request.get_json())

def getProductsCustoJusto(request):
    return getCustoJustoProducts(request.get_json())

def getProductsStandVirtual(request):
    return getStandVirtualProducts(request.get_json())
    