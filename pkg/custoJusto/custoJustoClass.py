class CustoJusto:
    def __init__(self, req):
        self.body = req

    def  __getitem__(self, x):
        mapper = {
            'brand': self.body['brand'],
            'model': self.body['model'],
            'category': self.body['category'],
            'sub_category': self.body['sub_category']
        }

        return mapper.get(x, x)

    def keys(self):
        return ['brand','model','category', 'sub_category']

    def getUrl(self): 
        this = dict(self)
        return 'https://www.custojusto.pt/portugal/{0}/{1}/{2}?o=[PAGE_NUMBER]'.format(this['category'], this['brand'], this['model']) 

    