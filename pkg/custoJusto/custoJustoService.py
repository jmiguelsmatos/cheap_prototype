from bs4 import BeautifulSoup
import requests
from logger import logger

from models.models import CustoJustoProduct
from pkg.helpers.proxys import random_proxy

from pkg.custoJusto.custoJustoClass import CustoJusto
from pkg.helpers.helper import (
    convertPriceToFloat,
    hasPageCustoJusto
)

productList = []

def getCustoJustoProducts(req: dict) -> []:

    cj = CustoJusto(req)

    CustoJustoProduct.objects().delete()

    return scrapCustoJusto(cj.getUrl(), 1)


def scrapCustoJusto(url, page):

    proxies = {
        'http' : random_proxy()
    }

    urlToScrap = url.replace('[PAGE_NUMBER]', str(page))

    try:
        response = requests.get(urlToScrap, proxies=proxies)
    except Exception as e:
        logger.exception("Exception occurred {0}".format(e))

    if response.url != urlToScrap:
        return productList

    soup = BeautifulSoup(response.content, "html.parser")

    resultSet = soup.find('div', class_="col-md-9")
    result_containers = resultSet.find_all('a')

    if len(result_containers) == 2:
        return productList

    for container in result_containers:
        if len(container.findChildren('img')) > 0 and container.find('h2'):

            link = container.get('href')
            name = container.find('h2').text.strip(
            ) if container.find('h2') else None
            price = container.find('h5').text.strip(
            ) if container.find('h5') else None
            provider = "CustoJusto"

            if container.findChildren('img')[0].get('data-src') is None:
                image = container.findChildren('img')[0].get('src')
            else:
                image = container.findChildren('img')[0].get('data-src')

            product = {
                'link': link,
                'name': name,
                'image': image,
                'price': convertPriceToFloat(price),
                'provider': provider
            }

            CustoJustoProduct(
                link=link,
                name=name, 
                image=image, 
                price=str(convertPriceToFloat(price)), 
                provider=provider
            ).save()

            productList.append(product)
            logger.warning('importing... {0}'.format(name))

    page += 1

    scrapCustoJusto(url, page)

    return productList
