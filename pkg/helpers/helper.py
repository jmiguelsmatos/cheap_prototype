def convertPriceToFloat(price) -> str:
    return price.replace('€','').replace(' ','').replace('.','').replace(',','').replace('EUR','')

def hasPage(url) -> bool:
    return None if "page" not in url else True

def hasPageCustoJusto(url) -> bool:
    return None if "o=" not in url else True