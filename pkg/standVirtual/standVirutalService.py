from bs4 import BeautifulSoup
import requests
from logger import logger

from models.models import StandVirtualProduct
from pkg.helpers.proxys import random_proxy

from pkg.standVirtual.standVirtualClass import StandVirtual
from pkg.helpers.helper import (
    convertPriceToFloat,
    hasPage
)

productList = []

def getStandVirtualProducts(request): 
    stv = StandVirtual(request)
    
    StandVirtualProduct.objects().delete()

    return scrapStandVirtual(stv.getUrl(), 1)


def scrapStandVirtual(url, page):
    proxies = {
        'http' : random_proxy()
    }

    urlToScrap = url.replace('[PAGE_NUMBER]', str(page))
    try:
        response = requests.get(urlToScrap, proxies=proxies)
    except Exception as e:
        logger.exception("Exception occurred {0}".format(e))

    
    if response.url != urlToScrap and hasPage(response.url):
        return productList

    soup = BeautifulSoup(response.content, "html.parser")

    resultSet = soup.find_all('article')
    
    for container in resultSet:

        link = container.get('data-href')
        name = container.find('a',class_="offer-title__link").text.strip()
        price = container.find('span',class_="offer-price__number").text.strip()
        image = container.find('img',class_="lazyload").get('data-src') if container.find('img',class_="lazyload") else None
        provider = 'StandVirtual'

        product = {
            'link' : link,
            'name' : name,
            'price': convertPriceToFloat(price),
            'image' : image,
            'provider': provider
        }

        StandVirtualProduct(
            link=link,
            name=name, 
            image=image, 
            price=str(convertPriceToFloat(price)), 
            provider=provider
        ).save()        
        
        productList.append(product)
        logger.warning('importing... {0}'.format(name))

    page += 1

    scrapStandVirtual(url, page)
        

    return productList