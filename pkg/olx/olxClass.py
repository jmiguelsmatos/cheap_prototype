class Olx:
    def __init__(self, req):
        self.body = req

    def  __getitem__(self, x):
        mapper = {
            'brand': self.body['brand'],
            'model': self.body['model'],
            'category': "carros-motos-e-barcos",
            'sub_category': self.body['sub_category']
        }
        return mapper.get(x, x)
        
    def keys(self):
        return ['brand','model','category', 'sub_category']

    def getUrl(self): 
        this = dict(self)
        return 'https://www.olx.pt/{0}/{1}/{2}/?search[filter_enum_modelo][]={3}&search[description]=1&page=[PAGE_NUMBER]'.format(this['category'], this['sub_category'], this['brand'], this['model']) 
