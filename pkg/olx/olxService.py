from bs4 import BeautifulSoup
import requests
from logger import logger

from models.models import OlxProduct
from pkg.helpers.proxys import random_proxy

from pkg.olx.olxClass import Olx
from pkg.helpers.helper import (
    convertPriceToFloat,
    hasPage
)

productList = []

def getOlxProducts(req: dict) -> []:

    olx = Olx(req)

    OlxProduct.objects().delete()

    return scrapOlx(olx.getUrl(), 1)

def scrapOlx(url, page):
    proxies = {
        'http' : random_proxy()
    }

    urlToScrap = url.replace('[PAGE_NUMBER]', str(page))

    try:
        response = requests.get(urlToScrap, proxies=proxies)
    except Exception as e:
        logger.exception("Exception occurred {0}".format(e))

    if response.url != urlToScrap and hasPage(response.url):
        return productList

    soup = BeautifulSoup(response.content, "html.parser")

    resultSet = soup.find_all('table', class_="offers")
    result_containers = resultSet[1].find_all('td', class_="offer")

    for container in result_containers:

        link = container.find('a').get('href') if container.find('a') else None
        name = container.find('strong').text.strip()
        price = container.find('p', class_="price").text.strip()
        image = container.find('img').get('src') if container.find('img') else None
        provider = "Olx"

        product = {
            'link': link,
            'name': name,
            'image': image,
            'price' : convertPriceToFloat(price),
            'provider': provider
        }

        OlxProduct(
            link=link,
            name=name, 
            image=image, 
            price=str(convertPriceToFloat(price)), 
            provider=provider
        ).save()
        
        productList.append(product)
        logger.warning('importing... {0}'.format(name))

    page += 1

    scrapOlx(url, page)

    return productList


