FROM python:3.7

COPY ./requirements.txt .
RUN apt-get update && \
    pip3 install -r ./requirements.txt
