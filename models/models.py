from flask_mongoengine import MongoEngine

db = MongoEngine()

class Product():
    link = db.StringField()
    name = db.StringField()
    image = db.StringField()
    price = db.StringField()
    provider = db.StringField()


class OlxProduct(db.Document, Product):
    meta = {
        'collection': 'products-olx'
    }

class CustoJustoProduct(db.Document, Product):
    meta = {
        'collection': 'products-custo-justo'
    }

class StandVirtualProduct(db.Document, Product):
    meta = {
        'collection': 'products-stand-virtual'
    }