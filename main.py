import os

from flask import (
    Flask,
    request,
    Response,
    jsonify
)
from flask_mongoengine import MongoEngine
from flask_cors import CORS

from controllers import productsController as ProductController
from errorHandler import InvalidUsage

app = Flask(__name__)
app.config.from_object(__name__)

app.config['MONGODB_SETTINGS'] = {
    'db': 'cheap-db',
    'host': 'mongo-py-cheap',
    'port': 27017
}

CORS(app)

from models.models import db
db.init_app(app)


@app.route('/product', methods=['POST'])
def products():
    prod = ProductController.getProducts(request)
    
    if len(prod) == 0:
        raise InvalidUsage('No Results found', status_code=204)

    return jsonify(prod)

@app.route('/product/<provider>', methods=['POST'])
def productsProvider(provider):
    # TODO: Solve bug always returns olx
    providers = {
        'olx': ProductController.getProductsOlx(request),
        'custoJusto': ProductController.getProductsCustoJusto(request),
        'standVirtual': ProductController.getProductsStandVirtual(request),
    }
    
    prod = providers.get(provider, 'not found')
    if len(prod) == 0 or type(prod) is str:
        raise InvalidUsage('No Results found', status_code=500)

    return jsonify(prod)

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
